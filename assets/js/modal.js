const bookNowbtns=Array.from(document.querySelectorAll(".bookNow"))
const myModal=document.getElementById("myModal")
const backdrop=document.getElementById("backdrop")
const mycloseArray=Array.from(document.querySelectorAll(".myclose"))

const backDropHandler=()=>{
    backdrop.classList.toggle("visible")
}
 
const hideModal =()=>{
    myModal.classList.remove('visible')
    backDropHandler()
}

const showModal=()=>{
    myModal.classList.add('visible')
    backDropHandler()
}


bookNowbtns.forEach((ele)=>{
    ele.addEventListener("click",showModal)
})
 
mycloseArray.forEach((ele)=>{
    ele.addEventListener("click",hideModal)
})

backdrop.addEventListener("click",hideModal)